#include <iostream>
#include <sstream>
#ifdef __GNUG__
#pragma implementation "wxpoem.h"
#endif

#include "oodle.h"
#include "tetris.h"

// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

using namespace std;


class MyApp : public wxApp
{
public:
	virtual bool OnInit();
	
};



class MyFrame : public wxFrame
{
	public:

	MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size);
	virtual ~MyFrame();

	void DoBegin();
	void DoPause();
	void DoStop();

	//void OnIdle(wxTimerEvent& WXUNUSED(event));
	void OnQuit(wxCommandEvent& event);
	/*void OnOpen(wxCommandEvent& event);
	void OnScroll(wxScrollEvent& event);
	void OnPlay(wxCommandEvent& event);
	void OnBegin(wxCommandEvent& event);
	void OnEnd(wxCommandEvent& event);
	void OnPause(wxCommandEvent& event);
	void OnStop(wxCommandEvent& event);
	void OnSpeed(wxCommandEvent& event);
	void OnLoop(wxCommandEvent& event);
	void OnColor(wxCommandEvent& event);*/

	DECLARE_EVENT_TABLE()

	enum
	{
		ID_Moves = 100
	/*	ID_Quit = 1,
		ID_Open,
		ID_Controls_Begin,
		ID_Controls_End,
		ID_Controls_Play,
		ID_Controls_Pause,
		ID_Controls_Stop,
		ID_Speed_Text,
		ID_Loop,
		ID_Color,
		ID_TIMER*/
	};

//	wxScrollBar *scroll;
//	wxTimer* m_timer;
//	wxChoice* loop, *color;
//	wxTextCtrl* speedctl;

};

BEGIN_EVENT_TABLE(MyFrame, wxFrame)
/*	EVT_MENU(ID_Quit, MyFrame::OnQuit)
	EVT_MENU(ID_Open, MyFrame::OnOpen)
	EVT_SCROLL(MyFrame::OnScroll)
	EVT_BUTTON(ID_Controls_Play, MyFrame::OnPlay)
	EVT_TIMER(ID_TIMER, MyFrame::OnIdle)
	EVT_BUTTON(ID_Controls_Begin, MyFrame::OnBegin)
	EVT_BUTTON(ID_Controls_Pause, MyFrame::OnPause)
	EVT_BUTTON(ID_Controls_Stop, MyFrame::OnStop)
	EVT_BUTTON(ID_Controls_End, MyFrame::OnEnd)
	EVT_TEXT(ID_Speed_Text, MyFrame::OnSpeed)
	EVT_CHOICE(ID_Color, MyFrame::OnColor)
	EVT_CHOICE(ID_Loop, MyFrame::OnLoop)*/
END_EVENT_TABLE()


IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	MyFrame *frame = new MyFrame( "PuzzleEdit", wxDefaultPosition, wxDefaultSize );
	frame->Show(TRUE);
	SetTopWindow(frame);
	
	return TRUE;
}
MyFrame::~MyFrame()
{
//	delete m_timer;
}

MyFrame::MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
: wxFrame((wxFrame *)NULL, -1, title, pos, size)
{
	SetStatusBarPane( -1 );
	CreateStatusBar();

	wxPanel *panel1 = new wxPanel(this, -1);
	//wxPanel *panel2 = new wxPanel(this, -1);
	
	wxBoxSizer *sizer2 = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer *szHoriz = new wxBoxSizer(wxHORIZONTAL);
	wxFlexGridSizer *sizer1 = new wxFlexGridSizer(12, 6, 0 , 0 );

	wxString btnTxt = "0";
	for (int i = 1; i < 12*6+1; i++)
	{
		sizer1->Add( new wxButton(panel1, i, btnTxt, wxDefaultPosition ) , 0, 0);
	}
	
	sizer2->Add(sizer1, 0, wxALIGN_CENTRE_HORIZONTAL );
	szHoriz->Add( new wxStaticText(panel1, -1, "Number of Moves: ", wxDefaultPosition),0, wxALIGN_CENTER );
	szHoriz->Add(new wxTextCtrl(panel1, ID_Moves, "", wxDefaultPosition, wxSize(10, -1) ), 0, wxALIGN_CENTER);
	sizer2->Add(szHoriz, 0, wxALIGN_CENTRE_HORIZONTAL );

	//sizer2->Add (new wxButton(panel1, 0, btnTxt, wxDefaultPosition ) , 0, wxALIGN_CENTER | wxALIGN_CENTRE_VERTICAL);
	panel1->SetSizer( sizer2 );
	panel1->SetAutoLayout( TRUE );
	sizer2->Fit( panel1 );

}

void MyFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
Close(TRUE);
}

