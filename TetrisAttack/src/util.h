#ifndef _UTIL_
#define _UTIL_

enum BlockType
{
	BLOCK_1,
	BLOCK_2,
	BLOCK_3,
	BLOCK_4,
	BLOCK_5,
	BLOCK_6,
	NUMBLOCKS
};

BlockType& operator++(BlockType& d, int);
BlockType& operator++(BlockType& d, int);
ostream& operator << (ostream& os, BlockType& v);

#endif