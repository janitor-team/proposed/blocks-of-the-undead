#ifndef _CFIELDSPRITE_
#define _CFIELDSPRITE_

#include "oodle.h"

//! Abstact class that defines some methods used for any sprite placed on a CPlayingField
class CFieldSprite : public CSprite
{
public:
	void setOffset(CVector offset) { CFieldSprite::offset = offset; }
	void moveBoardCords(int x, int y);
	void moveRelBoardCords(int dX, int dY) { setPos( screenCords( boardX + dX, boardY + dY ) ); boardX += dX; boardY += dY; }
	//void moveRelBoardCords(int dX, int dY, double vel);   //! Set destination block to dX, dY in board cords with velocity of vel
	CGotoEvent* moveRelBoardCords(int dX, int dY, double speed);

	virtual void update();                                //! Move sprite until it reaches dest

	CFieldSprite* getClone(int x, int y);
	int getBoardX() { return boardX; }
	int getBoardY() { return boardY; }
	int getBoardWidth() { return widthBoard; }
	int getBoardHeight() { return heightBoard; }
	int getDestType() { return dtype; }

protected:
	CFieldSprite(const string& fileName, int x, int y, int width, int height, int fieldWidth, int fieldHeight, CVector offset, bool alpha = false);
	CFieldSprite(CSurface *s, int x, int y, int width, int height, int fieldWidth, int fieldHeight, CVector offset);

	CFieldSprite(const CFieldSprite& b, int x, int y);			//! Clone constructor
	CFieldSprite(const CFieldSprite& b, int x, int y, CVector offset);  //! Clone constructor
	CFieldSprite(const CFieldSprite& b, int x, int y, int fieldWidth, int fieldHeight, CVector offset);  //! Clone constructor
	CVector screenCords(int x, int y);              //! Convert from board coordinates to screen coordiantes using the offset
	void setDest(CVector d);
	bool atDest();                                  //! Tells if the sprite is at dest (returns false if dest == ZERO_VECTOR)
	bool isMoving() { return ( velocity != ZERO_VECTOR ); }
private:
	int boardX, boardY;                //! Location of object in board cords
	int widthBoard, heightBoard;       //! Width and height of object in board cords
	int fieldHeight, fieldWidth;       //! Width and height of field in number of objects
	CVector offset;                    //! Vector to upper-lefthand corner of CPlayingField from (0,0) in screen cords
	CVector dest;                      //! Vector to destination in *board coordinates*
	int dtype;                    //! 
};

#endif