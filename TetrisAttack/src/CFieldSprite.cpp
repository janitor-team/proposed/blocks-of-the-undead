#include "tetris.h"

/*void CFieldSprite::moveRelBoardCords(int dX, int dY, double vel)
{
	setDest( screenCords( boardX + dX, boardY + dY ) );
	velocity.setY(vel);
	boardX += dX;
	boardY += dY;
}
*/

CGotoEvent* CFieldSprite::moveRelBoardCords(int dX, int dY, double speed)
{
	int x = boardX + dX;
	int y = boardY + dY;

	if (x < fieldWidth && x >= 0 && y < fieldHeight && y >= 0)
	{
		CVector sc = screenCords(boardX + dX, boardY + dY);

		//setDest( sc );
		CGotoEvent *e = new CGotoEvent(*this, sc, speed, false);
		appendEvent(e);

		boardX += dX;
		boardY += dY;
		return e;
	}
	return NULL;
}

void CFieldSprite::moveBoardCords(int x, int y)
{
	if (x < fieldWidth && x >= 0 && y < fieldHeight && y >= 0)
	{
		CVector sc = screenCords(x, y);

		setPos( sc );
		boardX = x;
		boardY = y;
	}
}

void CFieldSprite::update()
{
	CSprite::update();
	if (dest != ZERO_VECTOR)
	{
		if (atDest())  // we are at or beyond dest (note: this doesn't work so well if vel doesn't aim at dest)
		{
			pos = dest;   // "stick" to the dest.  if other behavior is desired, override this function, but make sure to call this function and then do your custom stuff
			dest = ZERO_VECTOR;
			velocity = ZERO_VECTOR;
			dest = ZERO_VECTOR;
		}
	}
}

CFieldSprite* CFieldSprite::getClone(int x, int y)
{
	CFieldSprite* clone = new CFieldSprite(*this, x, y);
	return clone;
}

CFieldSprite::CFieldSprite(CSurface *s, int x, int y, 
			 int width, int height, int fieldWidth, 
			 int fieldHeight, CVector offset)
			 : CSprite(ZERO_VECTOR, ZERO_VECTOR, s, "static"),
               boardX(x), boardY(y), offset(offset), dest(ZERO_VECTOR),
               widthBoard(width), heightBoard(height),
               fieldWidth(fieldWidth), fieldHeight(fieldHeight),
               dtype(NA)
{
	setRect(CRect(offset.getX(), offset.getY(), fieldWidth * getWidth()/widthBoard, fieldHeight * getHeight()/heightBoard));
	moveBoardCords(x, y);
}

CFieldSprite::CFieldSprite(const string& fileName, int x, int y, 
						   int width, int height, 
						   int fieldWidth, int fieldHeight, 
						   CVector offset, bool alpha)
                           : CSprite(ZERO_VECTOR, ZERO_VECTOR, fileName, "static", CRect(0,0,0,0), alpha),
                             boardX(x), boardY(y), offset(offset), dest(ZERO_VECTOR),
							 widthBoard(width), heightBoard(height),
							 fieldWidth(fieldWidth), fieldHeight(fieldHeight),
							 dtype(NA)
{
	setRect(CRect(offset.getX(), offset.getY(), fieldWidth * getWidth()/widthBoard, fieldHeight * getHeight()/heightBoard));
	moveBoardCords(x, y);
}

CVector CFieldSprite::screenCords(int x, int y)
{
	return (CVector(x * getWidth()/widthBoard, y * getHeight()/heightBoard) + offset);
}

void CFieldSprite::setDest(CVector d)
{
	dest = d;
	dtype = 0;
	if (pos.getY() < dest.getY())
	{
		dtype = dtype | static_cast<int> (BOTTOM);
	}
	else if (pos.getY() > dest.getY())
	{
		dtype |= static_cast<int> (TOP);
	}

	if(pos.getX() < dest.getX())
	{
		dtype |= static_cast<int> (RIGHT);
	}
	else if (pos.getX() > dest.getX())
	{
		dtype |= static_cast<int> (LEFT);
	}
}

bool CFieldSprite::atDest()
{
	bool valY = false, valX = false;
	if (velocity.getY() < 0)
	{
		if (pos.getY() <= dest.getY())
		{
			valY = true;
		}
	}
	else
	{
		if (pos.getY() >= dest.getY())
		{
			valY = true;
		}
	}

	if (velocity.getX() < 0)
	{
		if (pos.getX() <= dest.getX())
		{
			valX = true;
		}
	}
	else
	{
		if (pos.getX() >= dest.getX())
		{
			valX = true;
		}
	}
	return (valX && valY);
}

CFieldSprite::CFieldSprite(const CFieldSprite& b, int x, int y)
                           : CSprite(b, true, ZERO_VECTOR, ZERO_VECTOR),
                             boardX(x), boardY(y), offset(b.offset), dest(ZERO_VECTOR),
							 widthBoard(b.widthBoard), heightBoard(b.heightBoard),
							 fieldWidth(b.fieldWidth), fieldHeight(b.fieldHeight),
							 dtype(NA)

{
	moveBoardCords(x, y);  // have to do this _after_ CSprite(...) because the width and height need to be setup
}

CFieldSprite::CFieldSprite(const CFieldSprite& b, int x, int y, CVector offset)
                           : CSprite(b, true, ZERO_VECTOR, ZERO_VECTOR),
                             boardX(x), boardY(y), offset(offset), dest(ZERO_VECTOR),
							 widthBoard(b.widthBoard), heightBoard(b.heightBoard),
							 fieldWidth(b.fieldWidth), fieldHeight(b.fieldHeight),
							 dtype(NA)
{
	moveBoardCords(x, y);   // have to do this _after_ CSprite(...) because the width and height need to be setup
}

CFieldSprite::CFieldSprite(const CFieldSprite& b, int x, int y, int fieldWidth, int fieldHeight, CVector offset)
                           : CSprite(b, true, ZERO_VECTOR, ZERO_VECTOR),
                             boardX(x), boardY(y), offset(offset), dest(ZERO_VECTOR),
							 widthBoard(b.widthBoard), heightBoard(b.heightBoard),
							 fieldWidth(fieldWidth), fieldHeight(fieldHeight), dtype(NA)
{
	moveBoardCords(x, y);   // have to do this _after_ CSprite(...) because the width and height need to be setup
}
