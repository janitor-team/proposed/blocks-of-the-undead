#ifndef _CBLOCKSET_
#define _CBLOCKSET_

#include "CBlock.h"

#include "oodle.h"
#include "util.h"
#include <vector>
#include <string>
using namespace std;

//! This is a class to handle loading and storage of blocksets for insertion into CPlayingFields
class CBlockSet
{
public:

	CBlockSet(const vector<string> &fileNames);  //! file names of block's "static" animation (an animation with a single image)
	CBlockSet(const string &prefix);
	CBlockSet(list<CSurface*> &s);
	~CBlockSet();

	void loadBlockAnimation(const string &name, CAnimation* anim, BlockType type);

	CBlock* getBlock(int x, int y, int fieldWidth, int fieldHeight, CVector offset, BlockType type) const;	//! Construct a new block of type type (return clone of one of the blocks in this blockset)

private:
	CBlock* blocks[NUMBLOCKS];

};

#endif