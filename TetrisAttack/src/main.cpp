#include "tetris.h"

int main(int argc, char **argv)
{
	srand(time(NULL));
	try
	{
		list<CVector> offsets;
		offsets.push_back(CVector(379,57));
		CTetrisGame d(640,480,32, false, "Blocks of the Undead" );
		d.setTheme( new CTetrisTheme(), offsets);

		d.start();
		return 0;
	}
	catch (ex_Generic e)
	{

		ostringstream o;
		e.stackTrace(o);
		LOG(o.str(), 1, LOG_ERROR);
		return 1;
	}
}

