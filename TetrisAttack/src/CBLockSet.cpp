#include "tetris.h"
#include <sstream>
using namespace std;

CBlockSet::CBlockSet(const vector<string> &fileNames)
{
	if (fileNames.size() < NUMBLOCKS)
	{
		_THROWEX(ex_illegalArguments, "Too few filenames provided when attempting to load blockset", "CBlockSet", "CBlockSet", "fileNames = {" << &fileNames << "}");
	}

	for (BlockType i = BLOCK_1; i < NUMBLOCKS; i++)
	{
		int x = static_cast <int> (i);
		blocks[x] = new CBlock(fileNames[i], 0, 0, 0, 0, ZERO_VECTOR, i);
	}
}

CBlockSet::CBlockSet(const string &prefix)
{
	for (BlockType i = BLOCK_1; i < NUMBLOCKS; i++)
	{
		int x = static_cast <int> (i);
		ostringstream ss;
		ss << prefix << "block" << (x+1);

		blocks[x] = new CBlock(ss.str(), 0,0,0,0,ZERO_VECTOR, i);
	}
}

CBlockSet::CBlockSet(list<CSurface*> &s)
{
	if (s.size() != NUMBLOCKS)
		_THROWEX(ex_illegalArguments, "Number of surfaces does not match number of blocks",
		           "CBlockSet", "CBlockSet", "s = " << &s);

	list<CSurface*>::iterator i2 = s.begin();
	for (BlockType i = BLOCK_1; i < NUMBLOCKS; i++)
	{
		int x = static_cast <int> (i);
		blocks[x] = new CBlock( (*i2), 0,0,0,0,ZERO_VECTOR, i);
		i2++;
	}
}

CBlockSet::~CBlockSet()
{
	for (BlockType i = BLOCK_1; i < NUMBLOCKS; i++)
	{
		delete blocks[i];
	}
}

void CBlockSet::loadBlockAnimation(const string &name, CAnimation* anim, BlockType type)
{
	blocks[type]->addAnimation(name, anim);
}

CBlock* CBlockSet::getBlock(int x, int y, int fieldWidth, int fieldHeight, CVector offset, BlockType type) const
{
	CBlock* clone = static_cast<CBlock*> (blocks[type])->getClone(x,y,fieldWidth,fieldHeight, offset);
	return clone;
}