#ifndef _CPLAYINGFIELD_
#define _CPLAYINGFIELD_

#include <vector>
#include "oodle.h"
#include "CBlock.h"
#include "CBlockSet.h"
#include "CCursor.h"
#include "CBLockLoc.h"
#include "CTetrisTheme.h"

using namespace std;

//! Class to handle each of the two playing fields
class CPlayingField
{
public:
	CPlayingField(CVector pos, CBlockSet &set, CTetrisTheme* t, int speed);
	~CPlayingField();

	void draw(CSurface& s);					//! Draw the playing field

	// Game mechanics
	void swap();											//! Swaps the blocks under the cursor
	void moveCursor(int dX, int dY, double sec);			//! Move cursor to new location in certain amt. of time
	void moveCursorAbs(int x, int y, double sec);

	// Maintainence
	void setBackground(const string& fileName);				//! Set background of playing field
	void addBlock(int x, int y, BlockType type);            //! Add a block to the playing field
	void addBlock(int x, int y, bool active, BlockType type);
	void setBlockAnim(int x, int y, const string &name);
	void appendBlockEffect(int x, int y, int duration, CEffect::Effects effect);
	int addEventAllBlocks(CSequencedEvent *e);
	void appendBlockEvent(int x, int y, CSequencedEvent* e);
	void removeBlock(int x, int y);                         //! Remove a block
	void removeBlockEffect(CBlock* ptr, CSyncBlockerEvent* sync);
	void removeList( list<CPoint> &toRemove, CSyncBlockerEvent* sync );
	bool blockExists(int x, int y);
	void moveBlock(int x1, int y1, int x2, int y2);
	void moveBlockRelBoardCords(int x, int y, int dX, int dY, double speed) { if (blockExists(x, y)) blocks[x][y]->moveRelBoardCords(dX, dY, speed); }
	int getBlockX(CBlock *b);
	int getBlockY(CBlock *b);
	bool isGameOver() { return gameOver; }
	bool isGameWon() { return gameWon; }
	// Loading Boards
	void load(int stage, int index);
	void reLoadLevel() { load(lStage, lIndex); }
	int getStage() { return lStage; }
	int getLvl() { return lIndex; }
	CVector getPos() { return pos; }
	//Misc
	bool boardCords(CVector p, int &x, int &y);
	void clear();

	const static int FIELD_WIDTH = 6;
	const static int FIELD_HEIGHT = 12;
private:

	int width, height;
	int moveUpOffset;
	// Game mechanics
	void checkBlocks();										//! Checks for blocks, combos, etc.  (TODO: Some way to handle chains)
	void applyGravity();									//! Checks for floating blocks, gives them a velocity downwards
	void moveDown();                                        //! Moves a block down into 
	void moveUp();
	bool checkBounds(int x, int y);
	void setMoves(int m);
	bool isEmpty();
	bool allActive();
	bool allFallen();
//	CSurface* background;                                   //! Surface to hold the background of this field
	CBlock* blocks[FIELD_WIDTH][FIELD_HEIGHT];				//! 2-d array of pointers to block objects
	CBlockSet& blockSet;                                    //! Block set to get blocks from
	CCursor cursor;											//! Cursor sprite
//	vector<CGarbageBlock*> garbage;							//! vector that holds pointers to the garbage blocks in CPlayingField::blocks
	vector<CBlockLoc> clearedBlockLocations;				//! vector that holds locations of blocks just cleared (used for quickly finding if there are any combos or if it cleared garbage)
	list<CBlock*> freeBlocks;                               //! list that holds CBlocks that have been freed from the playing field, but that still need to be drawn
	Uint32 grav; 
	CVector pos;                                            //! Vector that holds location of upper-lefthand corner of the playing field
	CRect rect;                                             //! Rect that defines playing field's bounds
	CTimer moveUpTimer;                                     //! Timer to track when it is time to move the blocks up
	int moves;
	int numBlocks;
	CText movesTxt;
	CText stageTxt;
	CText gameOverTxt;
	int lStage, lIndex;
	bool gameOver, gameWon;
	CColor txtColor;
	CTetrisTheme* theme;
};

#endif
