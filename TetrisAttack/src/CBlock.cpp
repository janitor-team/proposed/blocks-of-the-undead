#include "tetris.h"
#include <string> 
using namespace std;

CBlock::CBlock(const CBlock& b, int x, int y)
        : CFieldSprite(b, x, y), active(b.active), type(b.type)
{ }

CBlock::CBlock(const CBlock& b, int x, int y, CVector offset)
        : CFieldSprite(b, x, y, offset), active(b.active), type(b.type)
{ }

CBlock::CBlock(const CBlock& b, int x, int y, int fieldWidth, int fieldHeight, CVector offset)
        : CFieldSprite(b, x, y, fieldWidth, fieldHeight, offset), active(b.active), type(b.type)
{ }

CBlock::CBlock(CSurface* s, int x, int y, int fieldWidth, int fieldHeight, CVector offset, BlockType type)
        : CFieldSprite(s, x,y, 1, 1, fieldWidth, fieldHeight, offset), type(type), active(true)
{ }

CBlock::CBlock(const string& fileName, int x, int y, int fieldWidth, int fieldHeight, CVector offset, BlockType type)
               : CFieldSprite(fileName + "_staticbg.png", x, y, 1, 1, fieldWidth, fieldHeight, offset), type(type), active(true)
{
	// compose block image onto surface
	CSurface *block = CSprite::getCurrentFrame();
	CSurface blockBG(*block);

	CSurface *img = CImgLoader::loadImage(fileName + "_staticimg.png", true);
	int w1 = block->getWidth();
	int h1 = block->getHeight();
	int w2 = img->getWidth();
	int h2 = img->getHeight();
	int x1 = (w1/2) - (w2/2);
	int y1 = (h1/2) - (h2/2);
	block->blit(*img, x1, y1);

	// now create all the animations
	list<CSurface*> s;
	for(int i = 15; i > 5; i--)
	{
		CSurface* tmp = new CSurface(blockBG);
		tmp->blitScaleEx(*img, CRect(x1,y1+(h2-i/20.*h2),0,0), 1, i/20.);
		s.push_back( tmp );
	}
	for(list<CSurface*>::reverse_iterator i = s.rbegin(); i != s.rend(); ++i)
	{
		s.push_back( (*i) );
	}
	addAnimation("hitanim_bottom", new CAnimation(s, 5));

	s.clear();
	for(int i = 15; i > 5; i--)
	{
		CSurface* tmp = new CSurface(blockBG);
		tmp->blitScaleEx(*img, CRect(x1+(w2-i/20.*w2),y1,0,0), i/20., 1);
		s.push_back( tmp );
	}
	for(list<CSurface*>::reverse_iterator i = s.rbegin(); i != s.rend(); ++i)
	{
		s.push_back( (*i) );
	}
	addAnimation("hitanim_right", new CAnimation(s, 5));
	
	s.clear();
	for(int i = 15; i > 5; i--)
	{
		CSurface* tmp = new CSurface(blockBG);
		tmp->blitScaleEx(*img, CRect(x1,y1,0,0), i/20., 1);
		s.push_back( tmp );
	}
	for(list<CSurface*>::reverse_iterator i = s.rbegin(); i != s.rend(); ++i)
	{
		s.push_back( (*i) );
	}
	addAnimation("hitanim_left", new CAnimation(s, 5));

	delete img;
}

void CBlock::clear()
{
	CSprite::clear();
	active = false;
}

void CBlock::update()
{
	CFieldSprite::update();
}

CBlock* CBlock::getClone(int x, int y)
{
	CBlock *clone = new CBlock(*this, x, y);
	
	return clone;
}

CBlock* CBlock::getClone(int x, int y, CVector offset)
{
	CBlock* clone = new CBlock(*this, x, y, offset);
	return clone;
}

CBlock* CBlock::getClone(int x, int y, int fieldWidth, int fieldHeight, CVector offset)
{
	CBlock* clone = new CBlock(*this, x, y, fieldWidth, fieldHeight, offset);
	return clone;
}

bool CBlock::isActive()
{
	return active && !isMoving() && events.size() < 1;
}

bool CBlock::canRemove()
{
	return active && !isMoving();
}

void CBlock::setActive(bool active)
{
	CBlock::active = active;
}

bool CBlock::deleteMe()
{
	if (effect)
	{
		return (effect->isDone());
	}
	else
	{
		return !isAlive();
	}
}

bool CBlock::sameType(const CBlock &b)
{
	return b.type == type;
}
