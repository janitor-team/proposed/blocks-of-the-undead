#include "tetris.h"

CTetrisGame::CTetrisGame(int screenX, int screenY, int bitDepth, bool fullscreen, CTetrisTheme *theme, const list<CVector> &fieldOffsets)
                         : CSDLGame(screenX, screenY, bitDepth, fullscreen), theme(theme), state(Splash), done(false)
						 , mouseloc(0,0)//, sound()
{
	if (theme)
	{
		for (list<CVector>::const_iterator i = fieldOffsets.begin(); i != fieldOffsets.end(); ++i)
		{
			fields.push_back(new CPlayingField((*i), *(theme->getBlockSet()), theme, 100) );
		}
	}
	else
	{
		_THROWEX(ex_illegalArguments, "NULL theme", "CTetrisGame", "CTetrisGame",
			        "[ screenX=" << screenX << ", screenY=" << screenY << ", bitDepth=" << bitDepth <<
					", fullscreen=" << fullscreen << ", theme={" << theme << "}, fieldOffsets={" << &fieldOffsets << "}]");
	}
	splash = CImgLoader::loadImage("splash640x480.png");
}

CTetrisGame::CTetrisGame(int screenX, int screenY, int bitDepth, bool fullscreen, const string &title)
                         : CSDLGame(screenX, screenY, bitDepth, fullscreen, title), theme(NULL), state(Splash), done(false)
						  , mouseloc(0,0)

{	
	splash = CImgLoader::loadImage("splash640x480.png");
}

CTetrisGame::~CTetrisGame()
{

    string file;
#if defined(WIN32) || defined(_WIN32)
	file = CImgLoader::getPath() + "/progress.txt";
#else
        char *homedir = getenv("HOME");
        if (homedir != NULL)
        {
            file = string(homedir) + "/.blocks-progress";
        }
        else
        {
            file = "blocks-progress";
        }
#endif

	ofstream prog ( file.c_str() , ios::trunc );
	if (prog && fields[0]->getStage() != 0 && fields[0]->getLvl() != 0)
	{
		if (state == Win)
		{
			prog << "1" << endl << "1" << endl;
		}
		else
		{
			prog << fields[0]->getStage() << endl;
			prog << fields[0]->getLvl() << endl;
		}
	}
	prog.close();

	delete splash;
#ifndef __GNUC__
	deleteContainer(fields);
	deleteContainer(sprites);
#else
        for (vector<CPlayingField*>::iterator i = fields.begin(); i != fields.end(); ++i)
        {
                delete (*i);
        }

        for (list<CSprite*>::iterator i = sprites.begin(); i != sprites.end(); ++i)
        {
                delete (*i);
        }
#endif

	delete theme;
}

void CTetrisGame::setTheme(CTetrisTheme* t, const list<CVector> &fieldOffsets)
{
	delete theme;
	theme = t;
#ifndef __GNUC__
	deleteContainer(fields);
#else
        for (vector<CPlayingField*>::iterator i = fields.begin(); i != fields.end(); ++i)
        {
                delete (*i);
        }
#endif
	for (list<CVector>::const_iterator i = fieldOffsets.begin(); i != fieldOffsets.end(); ++i)
	{
		fields.push_back(new CPlayingField((*i), *(theme->getBlockSet()), theme, 100) );
	}

	theme = t;
}

CSprite* CTetrisGame::addSprite(CVector velocity, CVector pos, const string &filename, CImgLoader::GraphicFileType fileType)
{
	CSprite *s = new CSprite(velocity, pos, filename, "static", CBounds(0,0,640,480) );
	sprites.push_front( s );
	return s;
}

CSprite* CTetrisGame::addSprite(CSprite* sprite)
{
	sprites.push_back( sprite );
	return sprite;
}

void CTetrisGame::animateSprites()
{
	for (list<CSprite*>::iterator i = sprites.begin(); i != sprites.end(); ++i)
	{
		CSprite *s = (*i);
		int w = (s->getWidth());
		int h = (s->getHeight());
		CRect r(0,0,640,480);
		if ( ! (s->getX() > -w && 
			s->getY()  > -h &&
			s->getY() < 480 &&
			s->getX() < 640))
		{
			list<CSprite*>::iterator tmp = i;

			if (tmp != sprites.end())
				tmp++;

			delete (*i);
			sprites.erase(i);
			i = tmp;
		}
		else
		{
			(*i)->update();
			(*i)->draw(*screen);
		}
	}
}

bool CTetrisGame::gameLoop(bool infinite)
{
	Uint32 t1 = 0;
		int ct = 26;
	SDL_ShowCursor(false);
	SDL_EnableKeyRepeat(25, 150);
	CTimer t;
	double fps = 0;
	bool b = false;
	while( !done )
	{
		t1 = SDL_GetTicks();
		SDL_Event event;
		while (!done && SDL_PollEvent(&event))
		{
			handleEvent(event);
		}
		
		switch(state)
		{
			case SinglePlayerPuzzle:
			{
				if (theme->getFloater() )
				{
					int w = theme->getFloater()->getWidth();
					if (fields[0]->getStage() == 1 && t.getElapsed() > 5000 && rand() % 50 > 48)
					{
						t.reset();
						CSprite *floater = theme->getFloater();
						CSprite *s = floater->getClone(CVector(-50, 0), CVector(639, rand() % 480) );
						addSprite( s );
					}

					if (fields[0]->getStage() == 2 && t.getElapsed() > 2000 && rand() % 50 > 48)
					{
						t.reset();
						CSprite *floater = theme->getFloater();
						CVector v[] = { CVector(123,352) , CVector(276,361), CVector(103,449) };
						CVector v1 = v[rand()%3];
						CSprite *s = floater->getClone(CVector(0, -50), v1);
						s->setScale(0.6);
						//s->setRot(PI/2);
						s->setRot(0);
						s->appendEvent( new CResizeEvent(*s, 0, .6, 5000, true) );
						//s->appendEvent( new CRotateEffect(1, 6000, *s, true) );
						s->appendEvent( new CRandomVelEvent( *s, .5, 1, 5, 0, true) );
						addSprite( s );
					}
				}
				screen->blit(*(theme->getBackground()), 0, 0);
				int stage = fields[0]->getStage();
				fields[0]->draw(*screen);
				animateSprites();
				theme->getCharacter()->draw(*screen);

				if (stage != fields[0]->getStage())
				{
					int newStage = fields[0]->getStage();
#ifndef __GNUC__
					deleteContainer(sprites);
#else
					for (list<CSprite*>::iterator i = sprites.begin(); i != sprites.end(); ++i)
					{
						delete (*i);
					}
#endif
					sprites.clear();
					delete theme;

					CTetrisTheme* tmptheme = new CTetrisTheme( stringify(fields[0]->getStage()) );
					CVector pos = fields[0]->getPos();
					delete fields[0];
					fields.clear();
					fields.push_back(new CPlayingField(pos, *(tmptheme->getBlockSet()), tmptheme, 100) );
					theme = tmptheme;
					fields[0]->load(newStage,1);
					state = Loading;
				}
				screen->doneDrawing();

				if (fields[0]->isGameWon())
				{
					state = Win;
				}
			} break;
			case Splash:
			{
				CText anykey("Press any key to start the game", CColor(255,255,255), CTextManager::getDefaultFont(), CTextManager::getPtSize(1.5));
				anykey.setPos(640-anykey.getWidth()-5, 0);
				screen->blit(*splash,0,0);
				anykey.draw(*screen);
				screen->doneDrawing();
			//	fields[0]->load(1,1);
			} break;
			case Loading:
			{
				CText t("Stage " + stringify(fields[0]->getStage()), CColor(255,255,255), CTextManager::getDefaultFont(), CTextManager::getPtSize(4), CText::BOLD);
				t.setPos(640/2-t.getWidth()/2,480/2-t.getHeight()/2);
				CText t2("Press any key to begin", CColor(255,255,255), CTextManager::getDefaultFont(), CTextManager::getPtSize(2));
				t2.setPos(640/2-t2.getWidth()/2, t.getPos().getY() + t.getHeight() + 6);
				screen->fill(CColor());
				t.draw(*screen);
				t2.draw(*screen);
				screen->doneDrawing();
			} break;
			case Win:
			{
				if (!b)
				{
					b = true;
					CText *t = new CText("WINNER!", CColor(255), CTextManager::getDefaultFont(), CTextManager::getPtSize(3), CText::BOLD);
					t->setRect(CRect(0,0,640,480));
					t->setVelocity(CVector(100,200));
					t->setPos(CVector(rand()%640, rand()%480));
					sprites.clear();
					addSprite(t);
				}
				else
				{
					screen->fill(CColor());
					animateSprites();
					screen->doneDrawing();
				}
			} break;

		}

		/*if (( (SDL_GetTicks() - t1)/1000. ) > 0)       // calculate FPS (used for sprite's framerate-independent movement)
			fps = 1 / ( (SDL_GetTicks() - t1)/1000. );
		
		ostringstream ss;
		ss << "FPS: " << (int)fps;
		ct++;
		if ( ct > 25 )
		{
			ct = 0;
			(static_cast<CText*> (*(sprites.begin())))->setText(ss.str());
		}*/
	}

	return true;
}

void CTetrisGame::handleEvent(const SDL_Event &e)
{
	switch (e.type)
	{
		case SDL_KEYDOWN:
			handleKeypress(e.key);
			break;
		case SDL_MOUSEMOTION:
			handleMouseMotion(e.motion);
			break;
		case SDL_MOUSEBUTTONDOWN:
			handleMouseButton(e.button);
			break;

		case SDL_QUIT:
			done = true;
			break;
	}
}

void CTetrisGame::handleKeypress(const SDL_KeyboardEvent &e)
{
	switch(state)
	{
		case SinglePlayerPuzzle:
		case SinglePlayer:
		{
			if (fields[0]->isGameOver())
			{
				fields[0]->reLoadLevel();
				break;
			}

			switch(e.keysym.sym)
			{
				case SDLK_ESCAPE:
					done = true;
					break;
				case SDLK_w:
					fields[0]->moveCursor(0, -1, 0);
					break;
				case SDLK_a:
					fields[0]->moveCursor(-1, 0, 0);
					break;
				case SDLK_s:
					fields[0]->moveCursor(0, 1, 0);
					break;
				case SDLK_d:
					fields[0]->moveCursor(1, 0, 0);
					break;
				case SDLK_r:
					fields[0]->reLoadLevel();
					break;
			        case SDLK_SPACE:
					fields[0]->swap();
					break;
			/*	case SDLK_RETURN:
					CTetrisTheme* tmptheme = new CTetrisTheme( stringify(3) );
					CVector pos = fields[0]->getPos();
					delete fields[0];
					fields.clear();
					fields.push_back(new CPlayingField(pos, *(tmptheme->getBlockSet()), tmptheme, 100) );
					theme = tmptheme;
					fields[0]->load(2,1);
					break;*/
			}
		} break;
		case Loading:
		{
			state = SinglePlayerPuzzle;
		} break;
		
		case Splash:
		{
			state = SinglePlayerPuzzle;
			int a = 1, b = 1;
			
                        string file;
#if defined(WIN32) || defined(_WIN32)
                        file = CImgLoader::getPath() + "/progress.txt";
#else
                        char *homedir = getenv("HOME");
                        if (homedir != NULL)
                        {
                            file = string(homedir) + "/.blocks-progress";
                        }
                        else
                        {
                            file = "blocks-progress";
                        }
#endif

			ifstream data( file.c_str() );
			if (data)
			{
				data >> a;
				data >> b;
			}

			if (a != 1)
			{
				CTetrisTheme* tmptheme = new CTetrisTheme( stringify(a) );
				CVector pos = fields[0]->getPos();
				delete fields[0];
				fields.clear();
				fields.push_back(new CPlayingField(pos, *(tmptheme->getBlockSet()), tmptheme, 100) );
				delete theme;
				theme = tmptheme;
			}
			fields[0]->load(a,b);
		}break;

		default:
		{
			switch(e.keysym.sym)
			{
				case SDLK_ESCAPE:
				{
					done = true;
					break;
				}
			}
			break;
		}
	}
}

void CTetrisGame::handleMouseMotion(const SDL_MouseMotionEvent &e)
{
	switch(state)
	{
		case SinglePlayerPuzzle:
		case SinglePlayer:
			mouseloc.setX(e.x);
			mouseloc.setY(e.y);
			int x, y;
			if ( (fields[0]->boardCords(mouseloc, x, y)))
			{
				fields[0]->moveCursorAbs(x, y, 0);
			}
			break;
	}
}

void CTetrisGame::handleMouseButton(const SDL_MouseButtonEvent &e)
{
	switch(state)
	{
		case SinglePlayerPuzzle:
		case SinglePlayer:
		{
			int x, y;
			bool cords;
			/*if ( (cords = fields[0]->boardCords(mouseloc, x, y)) && (fields[0]->blockExists(x, y) || fields[0]->blockExists(x+1, y)))
			{*/
				fields[0]->swap();
			//}
		}
	}
}
