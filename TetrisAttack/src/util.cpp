#include "tetris.h"

BlockType& operator++(BlockType& d, int)
{
  if (d == NUMBLOCKS) return d = BLOCK_1; //rollover
  int temp = d;
  return d = static_cast<BlockType> (++temp);
}

ostream& operator << (ostream& os, BlockType& v)
{
	switch (v)
	{
	case BLOCK_1:
		return( os << "Block 1" );
		break;
	case BLOCK_2:
		return( os << "Block 2" );
		break;
	case BLOCK_3:
		return( os << "Block 3" );
		break;
	case BLOCK_4:
		return( os << "Block 4" );
		break;
	case BLOCK_5:
		return( os << "Block 5" );
		break;
	default:
		return( os << "Unknown" );
		break;
	}
}