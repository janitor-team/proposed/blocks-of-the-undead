#ifndef _CBLOCK_
#define _CBLOCK_

#include <string>
#include "oodle.h"
#include "CFieldSprite.h"
#include "util.h"
using namespace std;

//! A class to hold individual blocks.
class CBlock : public CFieldSprite
{
public:
	CBlock(const string& fileName, int x, int y, int fieldWidth, int fieldHeight, CVector offset, BlockType type);  // fileName is the name of bitmap with static image for block
	CBlock(CSurface* s, int x, int y, int fieldWidth, int fieldHeight, CVector offset, BlockType type);

	void clear();						//! Clear this block (alive = false)
	void setActive(bool active);           //! Set this->active = active
//	void setOffset(CVector offset) { CBlock::offset = offset; }
//	void moveBoardCords(int x, int y) { setPos( screenCords(x, y) ); boardX = x; boardY = y; }
//	void moveRelBoardCords(int dX, int dY) { setPos( screenCords( boardX + dX, boardY + dY ) ); boardX += dX; boardY += dY; }
//	void moveRelBoardCords(int dX, int dY, double vel);
	void update();
//	void setDest(CVector d);

	bool deleteMe();                    //! Should this block be deleted?
	bool isActive();                    //! Is the block active?  (False when it is falling)
	bool canRemove();
	bool sameType(const CBlock &b);     //! Is this block the same type as block b?

	CBlock* getClone(int x, int y);    //! Get a clone at the specified point (in board cords)
	CBlock* getClone(int x, int y, CVector offset);    //! Get a clone at the specified point (in board cords)
	CBlock* getClone(int x, int y, int fieldWidth, int fieldHeight, CVector offset);    //! Get a clone at the specified point (in board cords)
	
private:
	CBlock(const CBlock& b, int x, int y);			//! Clone constructor
	CBlock(const CBlock& b, int x, int y, int fieldWidth, int fieldHeight, CVector offset);  //! Clone constructor
	CBlock(const CBlock& b, int x, int y, CVector offset);  //! Clone constructor

	//CVector screenCords(int x, int y);              //! Convert from board coordinates to screen coordiantes using the offset

	bool active;
	BlockType type;
	//int boardX, boardY;
	//CVector offset;                    //! Vector to upper-lefthand corner of CPlayingField from (0,0) in screen cords
//	CVector dest;
};

#endif