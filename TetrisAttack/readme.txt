Blocks of the Undead
--------------------

The objective is to clear all the blocks on the playing field.  Blocks are cleared when there are three of them (or more) in a row.  Also, "L-shaped" patterns can be formed, and all the blocks in the L will be cleared.  Move the white cursor around on the playing field by moving the mouse, and click to swap the two blocks under the cursor.  Press escape to exit, your progress will be saved.

There are three stages, each with six levels. Good luck!


- BOTUD team


Credits:
Jared Luxenberg - Lead Programmer/Project Leader
Korina Loumidi - Artwork and Sound Effects
Justin Lokey - Artwork, Sound Effects, Level Design
Keith Bare - Visual Effects/Linux compatibility