#include "CGotoAnimChangeEvent.h"
#include "CGotoEvent.h"
#include "exceptions.h"
#include "utils.h"

void CGotoAnimChangeEvent::doUpdate()
{
	if (!d)
	{
		setDone();
		return;
	}

	CGotoData* d2 = dynamic_cast<CGotoData*>(d);

	if (!d2)
	{
		setDone();
		return;
	}

	try
	{
		if (d2->hitBottom() && d2->hitLeft())
		{
			b.setAnimation("hitanim_bottom_left");
		}
		else if (d2->hitBottom() && d2->hitRight())
		{
			b.setAnimation("hitanim_bottom_right");
		}
		else if (d2->hitTop() && d2->hitLeft())
		{
			b.setAnimation("hitanim_top_left");
		}
		else if (d2->hitTop() && d2->hitRight())
		{
			b.setAnimation("hitanim_top_right");
		}
		else if(d2->hitBottom())
		{
			b.setAnimation("hitanim_bottom");
		}
		else if(d2->hitTop())
		{
			b.setAnimation("hitanim_top");
		}
		else if(d2->hitLeft())
		{
			b.setAnimation("hitanim_left");
		}
		else if(d2->hitRight())
		{
			b.setAnimation("hitanim_right");
		}
	}
	catch (ex_illegalArguments e)
	{
		LOG("failed to set animation of {" << this << "} (" << e.what() << ")", 5, LOG_INFO);
	}
	setDone();
}