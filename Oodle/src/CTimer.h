#ifndef _CTIMER_
#define _CTIMER_

//! Tick-based timer
class CTimer
{
public:
	CTimer();                      //! Construct timer that tracks time from the time it is created (or reset)
	CTimer(unsigned int period);   //! Construct timer that "rings" every [period] ticks
//	CTimer(const CTimer &t);       //! Copy constructor (copys timer and resets it)

	bool isRinging();              //! returns true when the timer is ringing and then resets the timer
	unsigned int getElapsed();     //! get elapsed time from start time
	unsigned int getElapsedReset();//! get elapsed time from start time and reset()
	
	void setPeriod(unsigned int p);//! sets period of this timer

	void reset();                  //! sets startTime = now

private:
	unsigned int startTime;
	unsigned int period;
};

#endif