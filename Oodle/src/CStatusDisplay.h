#ifndef _CSTATUSDISPLAY_
#define _CSTATUSDISPLAY_

#include <string>
using namespace std;

class CStatusDisplay
{
public:
	CStatusDisplay(bool bar = true, int max = 100);

	void hasProgress(bool bar = true);
	void setProgressMax(int max);
	void setProgress(double complete);
	void setMajorStatus(const string &s);
	void setMinorStatus(const string &s);
	
	void addMax(int i = 1);
	void addProgress(int i = 1);

	int getProgressMax();
	const string& getMajorStatus();
	const string& getMinorStatus();
	int getProgressCurrent();
	double getProgessFrac();

private:
	bool prog;
	int current, max;
	string majorStatus;
	string minorStatus;
};

#endif