#ifndef _CANIMATION_
#define _CANIMATION_

#include "CSurface.h"
#include "CTimer.h"
#include <list>
using namespace std;

/*! The animation class handles the loading of spite animations from big-ass
    bitmaps or from numbered files.  It also provides a SDL surface of the 
	current frame of the animation.
*/
class CAnimation
{
public:
	~CAnimation();									//! Must free all surfaces created and stored in frames list
	CAnimation(const string &fileName,				//! Filename and width and height and padding and speed (FPS) of BIGASS bitmap
				int width, int height, 
				int padding, int speed, bool alpha, bool nopath);
	CAnimation(const string &fileName,				//! Filename and width and height and padding and speed (FPS) of BIGASS bitmap
				int width, int height, 
				int padding, int speed, bool loop,
				bool alpha, bool nopath);
	CAnimation(const string &fileName,
		        bool alpha, bool nopath);				//! Filename of bitmap to load (ie this will be a static animation)
	CAnimation(CSurface* s);                        //! Create animation of one frame from given surface (and take ownership of this pointer)
	CAnimation(const list<CSurface*> &s, int period, bool loop = false);                     //! Create animation of any number of frames from given Surfaces (and take ownership of these ptrs)
	
	CAnimation(const CAnimation &a);                //! Copy constructor for deep copy
	const CAnimation& operator=(const CAnimation &s);   //! Assignment operator for deep copy

	void update();									//! updates animation's state based on current fps (must be called _every_ frame)
	void start() { animating = true;  }             //! starts the animation
	void pause() { animating  = false; }             //! stops the animation
	void reset() { currentFrame = frames.begin(); done = false; start(); }
	CSurface* getCurrentFrame() {                   //! returns ptr to surface containing current frame
		return *currentFrame;
	}; 
	int getFrames();
	bool isAnimating() { return animating; }
	void setFrame(int n);
	int getFrameIndex();
	void setSpeed(int s) { timer.setPeriod(s); }
	void setLoop(bool l = true) { loop = l; }
	CAnimation* getClone();                         //! returns clone of this animtion (new animation object backed by same list of surfaces)
	bool isDone() { return done; }
	void setAlpha(Uint32 flags, Uint8 alpha);

private:
	CAnimation(const CAnimation& sprite, bool clone);  //! Construct animation of given sprite if clone==true

	list<CSurface*> frames;                         //! list holding all the frames of the animation
	list<CSurface*>::iterator currentFrame;         //! iterator to iterate through list of frames
	int width;										//! Width of each frame
	int height;										//! Height of each frame
	bool animating;									//! Animate this sprite or not
	CTimer timer;                                   //! Timer to time animation
	bool isClone;                                   //! is this animation a clone of another?  (ie backed by same CSurface's)
	bool loop;                                      //! Loop this animation, or freeze on last frame?
	bool done;                                      //! is animation over (only valid if loop is false)
};

#endif
