#ifndef _CSDLGAME_
#define _CSDLGAME_

#include <string>
#include <vector>
#include <ctime>
#include "CAnimation.h"
#include "CImgLoader.h"
#include "CSprite.h"
#include "CVector.h"
using namespace std;

//! CSDLGame implements the game looop, tracks sprites, adds sprites, etc
class CSDLGame
{
public:
	//! A constructor
	/*! The constructor inits SDL and sets up the resolution/bit
	    depth of the screen.
		\param screenX x value of resolution
		\param screenY y value of resolution
		\param bitDepth bit depth of screen (in bits)
		\exception ex_sdlInitFailed { SDL initialization failed }
		\exception ex_sdlSurfaceCreationFailed { Changing resolution/bit-depth of screen failed }
		\exception ex_sdlGenFail { Something went wrong with SDL, check error string and error value }
	*/
	CSDLGame(int screenX, int screenY, int bitDepth, bool fullscreen = true, const string &title = "");

	//! A desctructor
	/*! Cleans up SDL */
	virtual ~CSDLGame();

/*	virtual void pause();
	virtual void stop();*/
	virtual void start();
	virtual void update() { gameLoop(false); }
	void resize(int screenX, int screenY, int bitDepth, bool fullscreen);

protected:
	virtual bool gameLoop(bool infinite = true) = 0;				//! Main game loop
	CSurface* screen;                //! Holds actual surface representing screen in memory
	
private:
/*	void animateSprites();			//! Animates sprites (based on the animation they are on)
	void moveSprites();				//! Moves sprites (based on velocity vector)*/
//	vector<CSprite*> sprites;		//! vector containing all active sprites (dead, invisible sprites are removed)
};

#endif
