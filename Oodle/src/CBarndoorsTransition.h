#ifndef _CBARNDOORSTRANSITION_
#define _CBARNDOORSTRANSITION_

#include "CTransition.h"
#include "CSprite.h"
#include "CSurface.h"

class CBarndoorsTransition : public CTransition
{
public:
	CBarndoorsTransition(const CSurface &a, const CSurface &b);

	void update();
	void draw(CSurface &dest);
	bool done();

private:
	CSprite leftDoor, rightDoor;
	const CSurface &b;
};

#endif