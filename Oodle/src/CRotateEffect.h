#ifndef _CROTATEEFFECT_
#define _CROTATEEFFECT_

class CRotateEffect : public CTimedEvent
{
public:
	CRotateEffect(int rotations, int time, CSprite &s, bool concurrent) : s(s), time(time), rotations(rotations), CTimedEvent(0, concurrent) {}
	CRotateEffect* getCopy() { return new CRotateEffect(*this); }

	void doUpdate();

private:
	CSprite &s;
	int rotations;
	unsigned int time;
};

#endif