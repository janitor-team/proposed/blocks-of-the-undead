#ifndef _CSIMPLEGAME_
#define _CSIMPLEGAME_

#include "CSDLGame.h"
#include "CSprite.h"
#include "utils.h"

class CSimpleGame : public CSDLGame
{
public:
	CSimpleGame(int screenX, int screenY, int bitDepth, bool fullscreen) : CSDLGame(screenX, screenY, bitDepth, fullscreen), bgcolor() {}
	~CSimpleGame()
        {
#ifndef __GNUC__
                deleteContainer(sprites);
#else
                for (list<CParticle*>::iterator i = sprites.begin(); i != sprites.end(); ++i)
                {
                        delete (*i);
                }
#endif
        }

	void addSprite(CParticle* s) { sprites.push_front(s); }
	void removeSprite(CSprite* s) { sprites.remove(s); }
	void setBGColor(CColor c) { bgcolor = c; }
	void x(CSurface* s) { CColor red = CColor(255); for (int x=0; x < 50; x++) for (int y=0; y<50;y++) s->setPixel(x,y,red);}
	bool gameLoop(bool infinite = true)
	{ 
		SDL_Event event;
		CTimer timer;
		int i = 0;
		do 
		{
			timer.reset();
			SDL_PollEvent(&event);
				switch (event.type)
				{
					case SDL_KEYDOWN:
						switch(event.key.keysym.sym)
						{
						case SDLK_ESCAPE:
							infinite = false;
							break;
						default:
							break;
						}
						break;
					case SDL_QUIT:
						infinite = false;
						break;
				}
			
			screen->fill(bgcolor);

			for(list<CParticle*>::iterator j = sprites.begin(); j != sprites.end(); ++j)
			{ 
				(*j)->update(); 
				(*j)->draw(*screen); 
			}
			screen->doneDrawing();
			i++;
			if (i)
			{
				i = 0;
			}
		} 
		while(infinite);
		return true;
	}

private:
	list<CParticle*> sprites;
	CColor bgcolor;
};

#endif
