#ifndef _CGOTOEVENT_
#define _CGOTOEVENT_

#include "CSequencedEvent.h"
#include "CVector.h"
#include "CEventData.h"
#include "CParticle.h"

class CGotoData : public CEventData
{
public:
	enum DestType
	{
		NA = 0,
		TOP = 2,
		BOTTOM = 4,
		LEFT = 8,
		RIGHT = 16
	};

	CGotoData(int dtype) : dtype(dtype) {}

	CGotoData* getCopy() { return new CGotoData(*this); }

	bool hitBottom() { return hasFlag(BOTTOM); }
	bool hitTop() { return hasFlag(TOP); }
	bool hitLeft() { return hasFlag(LEFT); }
	bool hitRight() { return hasFlag(RIGHT); }

private:
	int dtype;
	
	bool hasFlag(int f)
	{
		return ((dtype & f) == f);
	}
};

class CGotoEvent : public CSequencedEvent
{
public:
	CGotoEvent(CParticle &o, CVector dest, double speed, bool c);
	CGotoEvent* getCopy();

	void doUpdate();
	void initialize();

	CGotoData* getEventData();

private:
	CParticle &obj;    //! particle to move
	CVector dest;      //! Destination in _screen coordinates_
	CVector start;     //! Location particle started at for checking what kind of collision it was (bottom, top, left, right)
	double speed;
};


#endif