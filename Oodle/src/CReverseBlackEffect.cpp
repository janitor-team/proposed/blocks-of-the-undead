#include "oodle.h"

// Effect code contributed by Keith Bare

CReverseBlackEffect::CReverseBlackEffect(int duration, bool concurrent, const CSurface &fx)
                          : CEffect(0, concurrent), duration(duration), black(fx), src(fx)
{
	black.fill(COLOR_BLACK);
	black.setAlpha(SDL_SRCALPHA, SDL_ALPHA_OPAQUE);
}

CReverseBlackEffect* CReverseBlackEffect::getCopy() 
{
	return new CReverseBlackEffect(*this);
}

void CReverseBlackEffect::draw(CSurface &s, int x, int y)
{
	Uint32 tick = ticksElapsed();
	if (((double)duration-tick) <= 1)
	{
		s.blit(src, x, y);
		setDone();
		return;
	}

	black.setAlpha(SDL_SRCALPHA, (255*(((double)duration - tick) / duration)-1));
	s.blit(src, x, y);
	s.blit(black, x, y);
}
