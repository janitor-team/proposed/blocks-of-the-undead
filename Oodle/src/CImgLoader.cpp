#include "sys/stat.h"
#include "oodle.h"

CSurface* CImgLoader::loadImage(const string& fileName, bool alpha, bool nopath)
{
	LOG("Attempting to load image " << fileName, 3, LOG_INFO);
	try
	{
		string file = "";
		if (!nopath)
			file = (getPath() + "img/" + fileName);
		else
			file = fileName;

		CSurface* s = new CSurface( IMG_Load(file.c_str()) );
		if (alpha)
		{
			s->displayFormatAlpha();
		}
		else
		{
			s->displayFormat();
		}
		return s;
	}
	catch(ex_illegalArguments)
	{
		_THROWEX(ex_sdlImageLoadFailed, "SDL Image Load Failed", "CImgLoader", "loadImage", "fileName = " << fileName);
	}
}

list<CSurface*> CImgLoader::loadBigassBitmap(const string& fileName, int width, int height, int padding, bool alpha, bool nopath)
{
	LOG("Attempting to load bigass bitmap from file " << fileName << 
		" [ width=" << width << ", height=" << height << ", padding=" << padding << " ]", 3, LOG_INFO);
	
	string file = "";
	if (!nopath)
		file = (getPath() + fileName);
	else
		file = fileName;

	SDL_Surface* tmp = IMG_Load(file.c_str());
	if (!tmp)
	{
		_THROWEX(ex_sdlImageLoadFailed, "SDL Image Load Failed", "CImgLoader", "loadBigassBitmap",
			"fileName = " << fileName << ", width = " << width << ", height = " << height << ", padding = " << padding);
	}
	SDL_SetAlpha(tmp, 0, 0);

	CSurface surf(tmp);

	// first make sure image has correct dimensions
	int surfaceWidth = surf.getWidth();
	int surfaceHeight = surf.getHeight();
	
	if (padding != 0)
	{
		if ( ! ((surfaceWidth % width) % padding == 0) || ! ((surfaceHeight % height) % padding == 0) ) // the remainder of surfaceWidth / tileWidth should be some multiple of the padding
		{
			_THROWEX(ex_illegalArguments, "Illegal frame width, height, and/or padding for given file", "CImgLoader", "loadBigassBitmap",
				"fileName = " << fileName << ", width = " << width << ", height = " << height << ", padding = " << padding);
		}
	}
	else
	{
		if ( ! ((surfaceWidth % width) == 0) || ! ((surfaceHeight % height) == 0) ) // the remainder of surfaceWidth / tileWidth should be some multiple of the padding
		{
			_THROWEX(ex_illegalArguments, "Illegal frame width, height, and/or padding for given file", "CImgLoader", "loadBigassBitmap",
				"fileName = " << fileName << ", width = " << width << ", height = " << height << ", padding = " << padding);
		}
	}
	int numCols = surfaceWidth / width;
	int numRows = surfaceHeight / height;
	list<CSurface*> tmpList;

	for (int y = 0; y < numRows; y++)
	{
		for (int x = 0; x < numCols; x++)
		{
			int pixX = x * (width + padding);
			int pixY = y * (height + padding);
			
			CSurface* s2 = surf.getSection(pixX, pixY, width, height);
			if (alpha)
			{
				s2->displayFormatAlpha();
			}
			else
			{
				s2->displayFormat();
			}
			tmpList.push_back(s2);
		}
	}
	return tmpList;
}

string CImgLoader::getPath()
{
	const string testImg = "fonts/Vera.ttf";

	static string imgPath = "";
	struct stat st;

	if (imgPath != "")
	{
		return imgPath;
	}
	else
	{
#ifdef PKGDATADIR
		if (stat((PKGDATADIR + ("/assets/" + testImg)).c_str(), &st) == 0)
		{
			imgPath = PKGDATADIR + string("/assets/");
		}
		else if (stat(("./assets/" + testImg).c_str(), &st) == 0)
#else
		if (stat(("./assets/" + testImg).c_str(), &st) == 0)
#endif
		{
			imgPath = "./assets/";
		}
		else if (stat(("../assets/" + testImg).c_str(), &st) == 0)
		{
			imgPath = "../assets/";
		}
		else if (stat(("./" + testImg).c_str(), &st) == 0)
		{
			imgPath = "./";
		}
		else if (stat(("./TetrisAttack/assets/" + testImg).c_str(), &st) == 0)
		{
			imgPath = "./TetrisAttack/assets/";
		}
		else
		{
			LOG("Unable to locate the image path", 2, LOG_ERROR);
			_THROWEX(ex_sdlImageLoadFailed, "unable to locate the image path",
				 "CImgLoader", "getPath", "");
		}

		LOG("Using " << imgPath << " for loading images", 1, LOG_INFO);
		return imgPath;
	}
}
