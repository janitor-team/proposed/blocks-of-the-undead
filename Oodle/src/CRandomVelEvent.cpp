#include "CRandomVelEvent.h"

void CRandomVelEvent::doUpdate()
{
	if (CTimedEvent::updateTime()) 
	{
		if (((double)rand()/RAND_MAX) < prob) 
		{ 
			if ( ((double)rand()/RAND_MAX) < 0.5 && changeX != 0)
			{
				double change = ((double)rand()/RAND_MAX) * (((double)rand()/RAND_MAX) < 0.5 ? -1 : 1);
				p.changeVelocity(changeX*change , 0);
			} 
			else
			{
				p.changeVelocity(0, changeY* ((double)rand()/RAND_MAX) * (((double)rand()/RAND_MAX) < 0.5 ? -1 : 1));
			}
		}
	}
}