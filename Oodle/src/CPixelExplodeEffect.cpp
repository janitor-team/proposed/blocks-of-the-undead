#include "oodle.h"

CPixelExplodeEffect::CPixelExplodeEffect(int duration, bool concurrent, CRect r, CSprite &s)
	: CExplodeEffect(s, duration, minVel, randVel, r, concurrent)
{
}

CPixelExplodeEffect::CPixelExplodeEffect(CExplodeEffect &e)
	: CExplodeEffect(e)
{
	for (list<CParticle*>::iterator i = pieces.begin(); i != pieces.end(); ++i)
	{
		pieces.push_back( new CPixel(*((CPixel*)(*i))) );
	}
}

CExplodeEffect *CPixelExplodeEffect::getCopy()
{
	return new CPixelExplodeEffect(*this);
}

void CPixelExplodeEffect::initialize()
{
	CEffect::initialize();
	int sHeight = s.getCurrentFrame()->getHeight();
	int sWidth = s.getCurrentFrame()->getWidth();
	CVector center = CVector(sWidth/2, sHeight/2);
	CSurface *frame;

	// If the sprite's frame changes before it is unlocked, we may be
	// screwed. It shouldn't ever happen though, as s.update() should
	// never be called before frame is unlocked.
	frame = s.getCurrentFrame();
	frame->lock();

	for (int x = 0; x < sWidth; x++ )
	{
		for (int y = 0; y < sHeight; y++)
		{
			CVector p(x,y);
			CVector vel = (p - center);
			vel.setMag(100*(rand()%50/50.)+25);
			vel.setDir( vel.getDirRad() + (rand()%50/50.) * (rand()%50/50. > .5 ? -1 : 1));

			CColor pcolor;
			frame->getPixel(x, y, pcolor);
			CPixel* pixel = new CPixel(vel, CVector(x,y), pcolor);

			pieces.push_back( pixel );
			numPieces++;
		}
	}

	// See the note above.
	frame->unlock();
}

void CPixelExplodeEffect::doUpdate()
{
	CTimer timer;
	for (list<CParticle*>::iterator i = pieces.begin(); i != pieces.end(); ++i)
	{
		unsigned int t = ticksElapsed();
		if (t > duration) { setDone(); return; }

		(*i)->update();
	}
	if (gone >= numPieces) {
		setDone();
	}
}
