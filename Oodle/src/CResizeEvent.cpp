#include "CResizeEvent.h"

void CResizeEvent::doUpdate()
{
	unsigned int elapsed = timer.getElapsed();
	if (elapsed > time)
	{
		setDone();
	}
	else
	{
		double scale = minScale + (timer.getElapsed()/(double)time)*(maxScale-minScale);
		s.setScale(scale);
//		s.setPosRel(((scale * s.getWidth())-s.getWidth())/2, ((scale * s.getHeight())-s.getHeight())/2);
	}
}