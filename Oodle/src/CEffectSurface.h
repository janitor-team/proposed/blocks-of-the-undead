#ifndef _CEFFECTSURFACE_
#define _CEFFECTSURFACE_

class CEffectSurface : public CEffect
{
public:
	bool hasImage() { return true; }
	CSurface* getImage() { return &fx; }

protected:
	CEffectSurface(int ticksPerFrame, bool concurrent, const CSurface &fx) : CEffect(ticksPerFrame, concurrent), fx(fx) {}
	
	CSurface fx;
};

#endif