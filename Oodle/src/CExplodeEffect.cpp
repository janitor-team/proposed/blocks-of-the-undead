#include "oodle.h"

// Effect code contributed by Keith Bare

CExplodeEffect::CExplodeEffect(CSprite &s, int duration, double minVel, double randVel, const CRect &r, bool concurrent)
                               : CEffect(0, concurrent), s(s), duration(duration), gone(0), r(r), numPieces(0),
							     minVel(minVel), randVel(randVel)
{
}

CExplodeEffect::~CExplodeEffect()
{
	for (list<CParticle*>::iterator i = pieces.begin(); i != pieces.end(); ++i)
	{
		delete (*i);
	}
}

void CExplodeEffect::draw(CSurface &s, int x, int y)
{
	for (list<CParticle*>::iterator i = pieces.begin(); i != pieces.end(); ++i)
	{
		(*i)->setPosRel(x, y);
		(*i)->draw(s);
		if (r != CRect(0,0,0,0))
		{
			if ( !r.contains((*i)->getPos()) )
			{
				gone++;
				//cout << "out of bounds for {" << this << "} = {" << *i << "} [" << gone << "/" << numPieces << "]" << endl;
				delete *i;
				list<CParticle*>::iterator tmp = i;
				tmp++;
				pieces.erase(i);
				i = tmp;
			}
			else
			{
				(*i)->setPosRel(-x, -y);
			}
		}
		else
		{
			(*i)->setPosRel(-x, -y);
		}
	}
}
