#ifndef _CMENUBUTTON_
#define _CMENUBUTTON_

class CMenuButton
{
public:
	CMenuButton(CSprite *icon, const CColor &bgColor, const CColor &hoverColor, const string &text, const CVector &pos);

	void draw(CSurface &dest);
	void update();
	int getWidth();
	int getHeight();
	int getX();
	int getY();

private:
	CSprite *icon;
	CText *text;
	CVector pos;
};

#endif