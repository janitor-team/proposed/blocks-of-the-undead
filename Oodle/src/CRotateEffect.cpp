#include "oodle.h"

void CRotateEffect::doUpdate()
{
	unsigned int elapsed = ticksElapsed();
	if (elapsed > time) setDone();
	double rot = ((double)elapsed/time) * (rotations * 2 * PI);

	s.setRot(rot);
}
