#include "oodle.h"

CSDLGame::CSDLGame(int screenX, int screenY, int bitDepth, bool fullscreen, const string &title)
                   : screen(NULL)
{
	LOG("-------------\nPowered by Oodle v1.0\n-------------", 1, LOG_INFO);

	LOG("Initializing SDL library", 6, LOG_INFO);
	// initialize SDL
	if (SDL_Init( SDL_INIT_VIDEO ) < 0) {
		// clean up SDL
		SDL_Quit( );
		// throw exception
		_THROWEX(ex_sdlInitFailed, "SDL Initilization Failed", "CSDLGame", "CSDLGame",
			"screenX = " << screenX << ", screenY = " << screenY << ", bitDepth = " << bitDepth);
	}
	SDL_WM_SetCaption(title.c_str(), NULL);

	resize(screenX, screenY, bitDepth, fullscreen);
}

CSDLGame::~CSDLGame()
{
	delete screen;

	LOG("Shutting down SDL library", 6, LOG_INFO);
	SDL_Quit();
}

void CSDLGame::resize(int screenX, int screenY, int bitDepth, bool fullscreen)
{
	delete screen;

	int fs = 0;
	if (fullscreen) fs = SDL_FULLSCREEN;
	
	LOG("Changing to resolution " + stringify(screenX) + "x" + stringify(screenY), 1, LOG_INFO);

	SDL_Surface* surf;

	// set display resolution and bit depth
	surf = SDL_SetVideoMode(screenX, screenY, bitDepth,
	                          SDL_HWSURFACE |          // create surface in video ram
	                          SDL_DOUBLEBUF | fs          // create surface with dbl buffering
	                          );         // surface is full-screen
	/*LOG("Got surface with: [Hardware=" << ((surf->flags & SDL_HWSURFACE) == SDL_HWSURFACE) <<
		                 ", DblBuf="   << ((surf->flags & SDL_DOUBLEBUF) == SDL_DOUBLEBUF) <<
						 ", Fullscreen=" << ((surf->flags & fs) == fs) << "]", 1, LOG_INFO);*/

	if (!surf) {
		LOG("Creating hardware surface failed, falling back to software surface", 1, LOG_ERROR);
		// there was some problem creating surface
		// first try to create it in SW mode
		surf = SDL_SetVideoMode(screenX, screenY, bitDepth,
		                          SDL_SWSURFACE |       // create surface in system ram
		                          SDL_DOUBLEBUF |       // create surface with dbl buffering
		                          fs);      // surface is full-screen
		if (!surf) {
			LOG("Creating software surface failed", 2, LOG_ERROR);
			// software surface failed too, just quit
			// clean up SDL
			SDL_Quit();
			// throw exception
			_THROWEX(ex_sdlSurfaceCreationFailed, "SDL Surface Creation Failed", "CSDLGame", "CSDLGame", 
				"screenX = " << screenX << ", screenY = " << screenY << ", bitDepth = " << bitDepth);
		}
	}
	
//	try
//	{
		screen = new CSurface(surf);
//	}
//	background = CImgLoader::loadImage("castlebg.bmp");
//	started = time(NULL);
}

void CSDLGame::start()
{
	//try
	//{
		while(!gameLoop());
	//}
	//catch (ex_Generic e)
	//{
	//	_ADDTRACEINFO(e, "CSDLGame", "start", "");
	//	throw e;
	//}
}

/*void CSDLGame::start(int time)
{
	runtime = time;
}
*/
