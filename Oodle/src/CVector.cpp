#include "CVector.h"
#include <iostream>
using namespace std;

CVector::CVector(double x, double y)
{
	this->x = x;
	this->y = y;
}

CVector CVector::add(const CVector &b) const
{
	CVector c ( x + b.getX(),y + b.getY() );
	return c;
}

CVector CVector::subtract(const CVector &b) const
{
	CVector c ( x - b.getX(),y - b.getY() );
	return c;
}
double CVector::dot(const CVector &b) const
{
	return ( ( x * b.getX() ) + ( y * b.getY() ) );
}

CVector CVector::multiply(double d) const
{
	return CVector( (d * x), (d * y) );
}

CVector CVector::operator+=(CVector b)
{
       *this = add(b);
       return *this;
}

CVector CVector::operator+(const CVector &b) const
{
	return add(b);
}

CVector CVector::operator-(const CVector &b) const
{
	return subtract(b);
}

double CVector::operator*(const CVector &b) const
{
	// dot product
	return dot(b);
}

CVector CVector::operator*(double d) const
{
	// scalar product
	return multiply(d);
}

/*const CVector& CVector::operator=(CVector b)
{
	x = b.x;
	y = b.y;
	return *this;
}*/

bool CVector::operator==(const CVector &b) const
{
	if ( x == b.getX() && y == b.getY() )
		return true;
	else
		return false;
}

bool CVector::operator>(const CVector &b) const
{
	if ( x > b.getX() && y > b.getY() )
		return true;
	else
		return false;
}

bool CVector::operator<(const CVector &b) const
{
	if ( x < b.getX() && y < b.getY() )
		return true;
	else
		return false;
}

bool CVector::operator>=(const CVector &b) const
{
	if ( x >= b.getX() && y >= b.getY() )
		return true;
	else
		return false;
}

bool CVector::operator<=(const CVector &b) const
{
	if ( x <= b.getX() && y <= b.getY() )
		return true;
	else
		return false;
}

bool CVector::operator!=(const CVector &b) const
{
	return ! (b == *this);
}

ostream& operator << (ostream& os, const CVector& v) 
{
	// Output the vector's magnitude and direction
	return (os << "Mag: " << v.getMag() << " @ " << v.getDirDeg());
}

CVector ZERO_VECTOR = CVector(0,0);
