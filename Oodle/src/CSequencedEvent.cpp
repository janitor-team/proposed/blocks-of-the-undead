#include "CSequencedEvent.h"

CSequencedEvent::CSequencedEvent(bool c)
                 : done(false), concurrent(c), d(NULL), init(false), refCount(1)
{ 
	start();
}

CSequencedEvent::CSequencedEvent(const CSequencedEvent &e) 
                  : done(e.done), concurrent(e.concurrent), running(e.running), refCount(e.refCount)
{ 
	d = (e.d ? e.d->getCopy() : NULL); 
}

const CSequencedEvent& CSequencedEvent::operator=(const CSequencedEvent &e)
{
	if (&e != this)  // don't allow assignment to self
	{
		delete d;   // free old memory

		d = (e.d ? e.d->getCopy() : NULL);
		done = e.done;
		concurrent = e.concurrent;
		running = e.running;
		refCount = e.refCount;
	}
	return *this;
}

CSequencedEvent::~CSequencedEvent() 
{ 
	delete d;
}

void CSequencedEvent::update() 
{ 
	if(!init) 
	{ 
		init = true; 
		initialize();
	} 
	doUpdate();
}


void CSequencedEvent::passEventData(CEventData* d) 
{ 
	delete CSequencedEvent::d;
	CSequencedEvent::d = d; 
	gotEventData(); 
}