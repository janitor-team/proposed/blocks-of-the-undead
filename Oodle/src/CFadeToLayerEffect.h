#ifndef _CFADETOLAYEREFFECT_
#define _CFADETOLAYEREFFECT_

#include "CTimedEvent.h"

class CFadeToLayerEffect : public CTimedEvent
{
public:
	CFadeToLayerEffect(unsigned int duration, const CSurface &s1, const CSurface &s2, bool concurrent = false) : CTimedEvent(0,concurrent), duration(duration), s1(s1), s2(s2) {}
	
	void draw(CSurface &s, int x, int y) {
		Uint32 tick = ticksElapsed();
		if (((double)duration-tick) <= 0)
		{
			s.blit(s2, x, y);
			setConcurrent();
			return;
		}

		s2.setAlpha(SDL_SRCALPHA, (Uint8)(255-(255*(((double)duration - tick) / duration))));
		s.blit(s1, x, y);
		s.blit(s2, x, y);
	}
	void doUpdate() {}
	bool hasDraw() { return true; }

	CFadeToLayerEffect* getCopy() { return new CFadeToLayerEffect(*this); }

private:
	CSurface s1, s2;
	unsigned int duration;
};

#endif