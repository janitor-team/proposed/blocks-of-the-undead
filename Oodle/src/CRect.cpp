#include "oodle.h"

CRect::CRect(double x, double y, double width, double height)
             : x(x), y(y), width(width), height(height)
{ }

double CRect::getHeight()
{
	return height;
}

double CRect::getWidth()
{
	return width;
}

double CRect::getX()
{
	return x;
}

double CRect::getY()
{
	return y;
}

bool CRect::checkLeft(double x)
{
	return (x >= CRect::x);
}

bool CRect::checkRight(double x)
{
	return (x <= CRect::x + width);
}

bool CRect::checkTop(double y)
{
	return (y >= CRect::y);
}

bool CRect::checkBottom(double y)
{
	//cout << y << " <= " << CRect::y + height << endl;
	return (y <= CRect::y + height);
}

bool CRect::contains(double x, double y)
{
	return ( (checkLeft(x) && checkRight(x) ) &&
	         (checkTop(y) && checkBottom(y) ) );
}

bool CRect::contains(CVector v)
{
	double x = v.getX();
	double y = v.getY();

	return ( (checkLeft(x) && checkRight(x) ) &&
	         (checkTop(y) && checkBottom(y) ) );
}

bool CRect::operator!=(CRect b)
{
	return ! (b == *this);
}

bool CRect::operator==(CRect b)
{
	return (b.width == width && b.height == height && b.x == x && b.y == y);
}