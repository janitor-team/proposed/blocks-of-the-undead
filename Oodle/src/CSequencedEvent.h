#ifndef _CSEQUENCEDEVENT_
#define _CSEQUENCEDEVENT_

#include "CSurface.h"
#include "CEventData.h"
#include <deque>
#include <list>

using namespace std;

class CSequencedEvent
{
public:
	CSequencedEvent(bool c);
	CSequencedEvent(const CSequencedEvent &e);
	const CSequencedEvent& operator=(const CSequencedEvent &s);

	virtual ~CSequencedEvent();

	virtual CSequencedEvent* getCopy() = 0;

	virtual void start() { running = true;  }             //! Start the event
	virtual void stop()  { running = false; }             //! Stop the event
	virtual void doUpdate() = 0;
	virtual void initialize() { };
	void update();                                        //! Call this every frame
	
	virtual bool hasImage() { return false; }
	virtual CSurface* getImage() { return NULL; }
	virtual bool hasDraw() { return false; }
	virtual void draw(CSurface &s, int x, int y) {  }

	virtual CEventData* getEventData() { return NULL; }
	void passEventData(CEventData* d);

	bool isDone()        { return done;     }
	bool isConcurrent()  { return concurrent; }                //! Allow other sequenced events to execute while this one is executing? (the value of this may change i.e. concurrent events may be allowed after 1 second, 50 frames, etc)
	bool isRunning()     { return running;  }
	int minusRef() { return --refCount; }
	int plusRef() { return ++refCount; }
	int getRef() { return refCount; }

	typedef list<CSequencedEvent*> Container;
	typedef Container::iterator Iter;
	typedef Container::const_iterator ConstIter;

protected:
	void setDone(bool d = true) { done = d; if (done) stop(); }
	void setConcurrent(bool c = true) { concurrent = c; }
	virtual void gotEventData() { }
	CEventData* d;

private:
	bool running;
	bool done;
	bool concurrent;
	bool init;
	int refCount;
};

#endif