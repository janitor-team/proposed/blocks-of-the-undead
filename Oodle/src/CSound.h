#ifndef _CSOUND_
#define _CSOUND_

#include "SDL_mixer.h"

class CSound
{
public:
	CSound(const string &snd);
	~CSound();

	void setChannel(int c);
	int getChannel() const;
	Mix_Chunk* getChunk() const;

private:
	Mix_Chunk* data;
	int chan;
};

#endif
	
