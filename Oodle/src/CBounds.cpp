#include "CBounds.h"

CBounds::CBounds(double x, double y, double width, double height, CRect::Side flag)
                 : flag(flag), CRect(x, y, width, height)
{ }

bool CBounds::contains(double x, double y)
{
	if (!( BOTTOM & flag == BOTTOM ) && ( CRect::checkBottom(y) ))
	{
		return false;
	}
	if (!( TOP & flag == TOP )       && ( CRect::checkTop(y) ))
	{
		return false;
	}
	if (!( RIGHT & flag == RIGHT )   && ( CRect::checkRight(x) ))
	{
		return false;
	}
	if (!( LEFT & flag == LEFT )     && ( CRect::checkLeft(x) ))
	{
		return false;
	}
	return true;
}

/*CRect::Side CBounds::whichBound(double x, double y)
{
	CRect::Side s;
	if (!( BOTTOM & flag == BOTTOM ) && ( CRect::checkBottom(y) ))
	{
		s |= BOTTOM;
	}
	if (!( TOP & flag == TOP )       && ( CRect::checkTop(y) ))
	{
		s |= TOP;
	}
	if (!( RIGHT & flag == RIGHT )   && ( CRect::checkRight(x) ))
	{
		s |= RIGHT;
	}
	if (!( LEFT & flag == LEFT )     && ( CRect::checkLeft(x) ))
	{
		s |= LEFT;
	}
	return s;
}*/

bool CBounds::checkBottom(double y)
{
	return (!( BOTTOM & flag == BOTTOM ) && ( CRect::checkBottom(y) ));
}

bool CBounds::checkLeft(double x)
{
	return (!( LEFT & flag == LEFT )     && ( CRect::checkLeft(x) ));
}

bool CBounds::checkTop(double y)
{
	return (!( TOP & flag == TOP )       && ( CRect::checkTop(y) ));
}

bool CBounds::checkRight(double x)
{
	return (!( RIGHT & flag == RIGHT )   && ( CRect::checkRight(x) ));
}