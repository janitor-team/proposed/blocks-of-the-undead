#ifndef _CCOLOR_
#define _CCOLOR_

#include "SDL.h"

typedef unsigned char	Uint8;

class CColor
{
public:
	CColor(Uint8 r = 0, Uint8 g = 0, Uint8 b = 0, Uint8 a = 0) 
		: r(r), g(g), b(b), a(a)
		 { sdlcolor = ((SDL_GetVideoSurface() != NULL) ? (SDL_MapRGB(SDL_GetVideoSurface()->format, r, g, b)) : 0); }
//	CColor(const CColor &c) : r(c.r), g(c.g), b(c.b) {};  unnecessary
	
	Uint8 getR() const { return r; }
	Uint8 getG() const { return g; }
	Uint8 getB() const { return b; }
	Uint8 getA() const { return a; }
	SDL_Color getSDLColor();
	Uint32 getDispFormatColor();

	void setR(Uint8 r);
	void setG(Uint8 g);
	void setB(Uint8 b);
	void setA(Uint8 a);

	/*const CColor &operator=(const CColor &c)
	{
		r = c.r;
		g  = c.g;
		b = c.b;

		return *this;
	}*/

private:
	Uint8 r;
	Uint8 g;
	Uint8 b;
	Uint8 a;
	Uint32 sdlcolor;
};

extern CColor COLOR_BLACK;
extern CColor COLOR_WHITE;
extern CColor COLOR_TRANS;
extern CColor COLOR_RED;
extern CColor COLOR_GREEN;
extern CColor COLOR_BLUE;

#endif
