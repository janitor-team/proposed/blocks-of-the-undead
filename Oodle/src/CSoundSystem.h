#ifndef _CSOUNDSYSTEM_
#define _CSOUNDSYSTEM_

#include "CSound.h"
#include "SDL_mixer.h"

class CSoundSystem
{
public:
	CSoundSystem(int chunksize = 2048, int frequency = MIX_DEFAULT_FREQUENCY, int channels = 2, Uint16 format = AUDIO_S16);
	~CSoundSystem();

	void playSound(CSound &snd) const;
	void stopSound(const CSound &snd) const;
private:
	
};

#endif
