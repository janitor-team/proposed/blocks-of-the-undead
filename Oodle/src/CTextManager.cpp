#include "oodle.h"

string CTextManager::getDefaultFont(FontType t)
{
	string fontName = "";
	switch(t)
	{
	case PLAIN:
		fontName = defaultFont;
		break;
	case FANCY:
		fontName = defaultFancyFont;
		break;
	}

	return CImgLoader::getPath() + "/fonts/" + fontName;
}

double CTextManager::getPtSize(double scale)
{
	return basePtSz * scale;
}

const string CTextManager::defaultFont("Vera.ttf");
const string CTextManager::defaultFancyFont("");