#ifndef _CPIXEL_
#define _CPIXEL_

#include "CParticle.h"
#include "CVector.h"
#include "CColor.h"
#include "CRect.h"
#include "CSurface.h"

class CPixel : public CParticle
{
public:
	CPixel(CVector v, CVector p, const CColor &c);
	CPixel(CVector v, CVector p, const CColor &c, const CRect &r);
	CPixel(const CPixel &p);
	CPixel(const CPixel &p, const CColor &c);

	const CPixel &operator=(const CPixel &p);

	virtual void draw(CSurface &surface);
	void lockBeforeDraw(bool l);

	const CColor &getColor();
	void setColor(const CColor &c);

private:
	CColor color;
	bool lock;
};

#endif
