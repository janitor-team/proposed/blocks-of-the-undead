#ifndef _CSYNCBLOCKEREVENT_
#define _CSYNCBLOCKEREVENT_

#include "CSequencedEvent.h"

class CSyncBlockerEvent : public CSequencedEvent
{
public:
	CSyncBlockerEvent(int n) : CSequencedEvent(false), total(n), i(0) {}
	
	CSyncBlockerEvent* getCopy() { return new CSyncBlockerEvent(*this); }

	void gotEventData() { if (++i == total) { setDone(); } }
	void setTotal(int n) { total = n; }
	void doUpdate() { }
	bool hasDraw() { return true; }

protected:
	int total, i;
};

#endif