#include "CStatusDisplay.h"


CStatusDisplay::CStatusDisplay(bool bar, int max)
                : prog(bar), current(0), max(max)
{ }

void CStatusDisplay::hasProgress(bool bar)
{
	prog = bar;
}

void CStatusDisplay::setProgressMax(int max)
{
	CStatusDisplay::max = max;
}

void CStatusDisplay::setMinorStatus(const string &s)
{
	minorStatus = s;
}

void CStatusDisplay::setMajorStatus(const string &s)
{
	majorStatus = s;
}

void CStatusDisplay::addMax(int i)
{
	max += i;
}

void CStatusDisplay::addProgress(int i)
{
	if (current < max) current += i;
}

const string& CStatusDisplay::getMajorStatus()
{
	return majorStatus;
}

const string& CStatusDisplay::getMinorStatus()
{
	return minorStatus;
}

int CStatusDisplay::getProgressMax()
{
	return max;
}

int CStatusDisplay::getProgressCurrent()
{
	return current;
}

double CStatusDisplay::getProgessFrac()
{
	return (double)current/max;
}
