#ifndef _CPLAYSOUNDEVENT_
#define _CPLAYSOUNDEVENT_

#include "CSequencedEvent.h"
#include "CSoundSystem.h"
#include "CSound.h"

class CPlaySoundEvent : public CSequencedEvent
{
public:
	CPlaySoundEvent(CSound &s, CSoundSystem &sm, bool concurrent = true) : CSequencedEvent(concurrent), s(s), sm(sm) {}

	CPlaySoundEvent* getCopy() { return new CPlaySoundEvent(*this); }

	void doUpdate() { sm.playSound(s); setDone();  }

private:
	CSound &s;
	CSoundSystem &sm;
};

#endif
