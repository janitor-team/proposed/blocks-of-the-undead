#ifndef _CREMOVESEQEVENT_
#define _CREMOVESEQEVENT_

#include "CSequencedEvent.h"

class CRemoveSeqEvent : public CSequencedEvent
{
public:
	CRemoveSeqEvent(CSequencedEvent* e, CParticle &p) : CSequencedEvent(true), p(p), e(e) {}
	CRemoveSeqEvent* getCopy() { return new CRemoveSeqEvent(*this); }
	void doUpdate() { p.removeEvent( e ); }

private:
	CParticle &p;
	CSequencedEvent *e;
};

#endif