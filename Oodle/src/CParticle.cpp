#include "oodle.h"

CParticle::CParticle(CVector v, CVector pos)
	: velocity(v), pos(pos), box(CRect(0,0,0,0)), timer(), dead(false)
{
}

CParticle::CParticle(CVector v, CVector pos, const CRect &r)
        : velocity(v), pos(pos), box(r), timer(), dead(false)
{
}

CParticle::CParticle(const CParticle &p)
	: velocity(p.velocity), pos(p.pos), moving(p.moving), timer(p.timer), box(p.box), dead(p.dead)
{
	// copy over all the events (need to make copies, not just copy pointers)
	for (CSequencedEvent::ConstIter i = p.events.begin(); i != p.events.end(); ++i)
	{
		CSequencedEvent* e = (*i)->getCopy();
		events.push_back(e);
	}

}

CParticle::~CParticle()
{
	for (CSequencedEvent::Iter i = events.begin(); i != events.end(); ++i)
	{
		if( (*i)->minusRef() == 0)
			delete *i;
	}
}

void CParticle::update()
{
	CEventData* d = NULL;
	bool concurrent = false;

	/*if (dynamic_cast<CBlock*> (this) != NULL && (dynamic_cast<CBlock*> (this))->getBoardY() < 11 && getVel() == ZERO_VECTOR)
		cout << "zero   {" << this << "} [" << velocity << "]" << endl;*/

	pos += velocity * (timer.getElapsedReset()/1000.); // dP = v * dT

	for (CSequencedEvent::Iter i = events.begin(); i != events.end(); ++i)
	{
		if ((*i)->isDone())
		{
			d = (*i)->getEventData();
			
			CSequencedEvent::Iter tmp = i;
			if (tmp != events.end())
				tmp++;

			if( (*i)->minusRef() == 0)
				delete (*i);

			events.erase(i);
			i = tmp;
		//	i++;
			if (i != events.end())   // have to go ahead and evaluate the i element here because if we let the loop run again, it would skip an event
			{
				(*i)->passEventData(d);   // event data is a way for sequenced events to pass information
				
				(*i)->update();
			
				if ( !(*i)->isConcurrent() )
				{
					concurrent = true;
					break;
				}
				d = (*i)->getEventData();
			}
			else
			{
				delete d;
				d = NULL;
			}
		}
		else
		{
			//(*i)->passEventData(d);
			(*i)->update();
			//d = (*i)->getEventData();
			
			if ( !(*i)->isConcurrent() && !(*i)->isDone() )   // if the event is not concurrent, break because it must run to completion
			{
				concurrent = true;
				break;
			}
			else if ((*i)->isDone())
			{
				d = (*i)->getEventData();
				
				CSequencedEvent::Iter tmp = i;

				if (tmp != events.end())
					tmp++;

				if( (*i)->minusRef() == 0)
					delete (*i);

				events.erase(i);
				i = tmp;
			//	i++;
				if (i != events.end())   // have to go ahead and evaluate the i element here because if we let the loop run again, it would skip an event
				{
					(*i)->passEventData(d);   // event data is a way for sequenced events to pass information
					(*i)->update();
				
					if ( !(*i)->isConcurrent() )
					{
						concurrent = true;
						break;
					}
				}
				else
				{
					delete d;
					d = NULL;
				}
			}
		}
	}

	if (!concurrent)
	{
		for (CSequencedEvent::Iter i = behaviors.begin(); i != behaviors.end(); ++i)
		{
			if ((*i)->isDone())
			{
				d = (*i)->getEventData();
				
				CSequencedEvent::Iter tmp = i;
				if (tmp != events.end())
					tmp++;

				if( (*i)->minusRef() == 0)
					delete (*i);

				events.erase(i);
				i = tmp;

				if (i != events.end())   // have to go ahead and evaluate the i element here because if we let the loop run again, it would skip an event
				{
					(*i)->passEventData(d);   // event data is a way for sequenced events to pass information

					(*i)->update();
				
					if ( !(*i)->isConcurrent() )
					{
						concurrent = true;
						break;
					}
					d = (*i)->getEventData();
				}
				else
				{
					d = NULL;
				}
			}
			else
			{
				//(*i)->passEventData(d);
				(*i)->update();
				//d = (*i)->getEventData();
				
				if ( !(*i)->isConcurrent() && !(*i)->isDone() )   // if the event is not concurrent, break because it must run to completion
				{
					concurrent = true;
					break;
				}
				else if ((*i)->isDone())
				{
					d = (*i)->getEventData();
					
					CSequencedEvent::Iter tmp = i;

					if (tmp != events.end())
						tmp++;

					if( (*i)->minusRef() == 0)
						delete (*i);

					events.erase(i);
					i = tmp;
				//	i++;
					if (i != events.end())   // have to go ahead and evaluate the i element here because if we let the loop run again, it would skip an event
					{
						(*i)->passEventData(d);   // event data is a way for sequenced events to pass information
						(*i)->update();
					
						if ( !(*i)->isConcurrent() )
						{
							concurrent = true;
							break;
						}
					}
					else
					{
						delete d;
						d = NULL;
					}
				}
			}
		}
	}



	stayInBoundary();

}

CParticle::BoundaryInfo CParticle::inBoundaryHoriz()
{
	return inBoundaryHoriz(pos.getX());
}

CParticle::BoundaryInfo CParticle::inBoundaryHoriz(double x)
{
	if (box != CRect(0,0,0,0))
	{
		if (! box.checkLeft(x))
		{
			return OUTSIDE_LOW;
		}
		else if (! box.checkRight(x))
		{
			return OUTSIDE_HIGH;
		}
		else
		{
			return INSIDE;
		}
	}
	else
	{
		return INSIDE;
	}
}

void CParticle::clear()
{
	dead = true;
}

CParticle::BoundaryInfo CParticle::inBoundaryVert()
{
	return inBoundaryVert(pos.getY());
}

CParticle::BoundaryInfo CParticle::inBoundaryVert(double y)
{
	if (box != CRect(0,0,0,0))
	{
		if (! box.checkBottom(y))
		{
			return OUTSIDE_HIGH;
		}
		else if (! box.checkTop(y))
		{
			return OUTSIDE_LOW;
		}
		else
		{
			return INSIDE;
		}
	}
	else
	{
		return INSIDE;
	}
}

void CParticle::stayInBoundary()
{
	// Check the horizontal position
	switch (inBoundaryHoriz())
	{
	case OUTSIDE_LOW:
		// Only bounce once
		if (velocity.getX() < 0)
		{
			cout << "bounce x for low" << endl;
			velocity.setX(-velocity.getX());
			pos.setX( box.getX());
		}
		break;
	case OUTSIDE_HIGH:
		// Only bounce once
		if (velocity.getX() > 0)
		{
			cout << "bounce x for high" << endl;
			velocity.setX(-velocity.getX());
			pos.setX( box.getX() + box.getWidth());
		}
		break;
	default:
		break;
	}

	switch (inBoundaryVert())
	{
	case OUTSIDE_LOW:
		// Only bounce once
		if (velocity.getY() < 0)
		{
			velocity.setY(-velocity.getY());
			pos.setY( box.getY());
		}
		break;
	case OUTSIDE_HIGH:
		// Only bounce once
		if (velocity.getY() > 0)
		{
			velocity.setY(-velocity.getY());
			pos.setY( box.getY() + box.getHeight());
		}
		break;
	default:
		break;
	}
}

void CParticle::setPos(CVector pos)
{
	if (inBoundaryHoriz(pos.getX()) == INSIDE)
	{
		CParticle::pos.setX(pos.getX());
	}

	if (inBoundaryVert(pos.getY()) == INSIDE)
	{
		CParticle::pos.setY(pos.getY());
	}
}

void CParticle::setPos(double x, double y)
{
	if (inBoundaryHoriz(pos.getX()) == INSIDE)
	{
		CParticle::pos.setX(x);
	}

	if (inBoundaryVert(pos.getY()) == INSIDE)
	{
		CParticle::pos.setY(y);
	}
}

void CParticle::setPosRel(double x, double y)
{
	if (inBoundaryHoriz(pos.getX()) == INSIDE)
	{
		CParticle::pos.addX(x);
	}

	if (inBoundaryVert(pos.getY()) == INSIDE)
	{
		CParticle::pos.addY(y);
	}
}

void CParticle::appendEvent(CSequencedEvent* e)
{
	events.push_back(e);
}

void CParticle::appendBehavior(CSequencedEvent* e)
{
	behaviors.push_back(e);
}

void CParticle::prependEvent(CSequencedEvent* e)
{
	//events.push_front(e);
}