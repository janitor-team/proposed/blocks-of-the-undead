#ifndef _CEXPLODEEFFECT_
#define _CEXPLODEEFFECT_

#include <list>
using namespace std;

class CExplodeEffect : public CEffect
{
public:
	virtual ~CExplodeEffect();
	virtual CExplodeEffect* getCopy() = 0;

	virtual void initialize() = 0;

	virtual void doUpdate() = 0;
	bool hasDraw() { return true; }

	void draw(CSurface &s, int x, int y);

protected:
	CExplodeEffect(CSprite &s, int duration, double minVel, double randVel, const CRect &r, bool concurrent);

	unsigned int duration;
	int gone, numPieces;
	double minVel, randVel;

	CRect r;
	CSprite &s;
	list<CParticle*> pieces;
};

#endif
